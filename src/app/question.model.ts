export interface AnswerKey {
  [key: string]: any;
}

export interface Question {
  itemId: string;
  stem: string;
  optionA: string;
  optionB: string;
  optionC: string;
  optionD: string;
  optionE: string;
  type: string;
  processedAnswers: string; // TODO: Fixme, the proper type is object literal
  exhibitId: string;
  exhibitPlacement: string;
  exhibitSplit: string;
  stemImage: string;
  imagePlacement: string;
  minOptions: string;
  maxOptions: string;
  supressOptionLabels: string;
  stemHeader: string;
  stemFooter: string;
}
