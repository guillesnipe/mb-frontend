import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';

import { MatSnackBar } from '@angular/material';

import { Question } from '../../question.model';
import { QuestionService } from '../../question.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id: string; // Question id
  question: any = {}; // It holds the question that we got from the db. Todo: Set the proper type HttpResponse<Question>
  answers: Array<any> = []; // Answers chosen by the user.
  defaultAnswers = this.questionService.defaultAnswers;
  updateForm: FormGroup;
  debugMode = true; // Debug flag set it to false for production. TODO: This should be coming from environment.ts

  constructor(private questionService: QuestionService,
              private route: ActivatedRoute,
              private snackBar: MatSnackBar,
              private fb: FormBuilder) {
                this.createForm();
              }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.questionService.getQuestionsById(this.id).subscribe(res => {
        // Getting the question
        this.question = res;
        // Getting the answers and retrieving user selection
        this.answers = this.getAnswers(this.defaultAnswers, JSON.parse(this.question.processedAnswers));
        // updating form values
        this.updateForm.get('itemId').setValue(this.question.itemId);
        this.updateForm.get('stem').setValue(this.question.stem);
        this.updateForm.get('optionA').setValue(this.question.optionA);
        this.updateForm.get('optionB').setValue(this.question.optionB);
        this.updateForm.get('optionC').setValue(this.question.optionC);
        this.updateForm.get('optionD').setValue(this.question.optionD);
        this.updateForm.get('optionE').setValue(this.question.responsible);
        this.updateForm.get('type').setValue(this.question.type);
        this.updateForm.controls.answerKey.patchValue(this.answers);
        this.updateForm.get('exhibitId').setValue(this.question.exhibitId);
        this.updateForm.get('exhibitPlacement').setValue(this.question.exhibitPlacement);
        this.updateForm.get('exhibitSplit').setValue(this.question.exhibitSplit);
        this.updateForm.get('stemImage').setValue(this.question.stemImage);
        this.updateForm.get('imagePlacement').setValue(this.question.imagePlacement);
        this.updateForm.get('minOptions').setValue(this.question.minOptions);
        this.updateForm.get('maxOptions').setValue(this.question.maxOptions);
        this.updateForm.get('supressOptionLabels').setValue(this.question.supressOptionLabels);
        this.updateForm.get('stemHeader').setValue(this.question.stemHeader);
        this.updateForm.get('stemFooter').setValue(this.question.stemFooter);
      });
    });
  }

  createForm() {
    this.updateForm = this.fb.group({
      itemId: ['', Validators.required],
      stem: ['', Validators.required ],
      optionA: ['', Validators.required ],
      optionB: ['', Validators.required ],
      optionC: '',
      optionD: '',
      optionE: '',
      type: ['', Validators.required ],
      answerKey: this.questionService.buildAnswers(this.defaultAnswers, 'selected', this.fb),
      exhibitId: '',
      exhibitPlacement: '',
      exhibitSplit: '',
      stemImage: '',
      imagePlacement: '',
      minOptions: ['', Validators.max(25)],
      maxOptions: ['', Validators.max(25)],
      supressOptionLabels: '',
      stemHeader: '',
      stemFooter: ''
    });
  }

  get answerKey() {
    return this.updateForm.get('answerKey');
  }

  // This method retrieves an array of question objects with the values selected by the user.
  getAnswers(defaultAnswers, processedAnswers) {
    const temp = defaultAnswers.slice();
    const answers = [...temp];

    processedAnswers.forEach( (answer) => {
      answers[answer.id].selected = true;
    });

    return answers.map(answer => answer.selected);
  }

  updateQuestion(itemId,
              stem,
              optionA,
              optionB,
              optionC,
              optionD,
              optionE,
              type,
              answerKey,
              exhibitId,
              exhibitPlacement,
              exhibitSplit,
              stemImage,
              imagePlacement,
              minOptions,
              maxOptions,
              supressOptionLabels,
              stemHeader,
              stemFooter) {
    this.questionService.updateQuestion(this.id,
                                        itemId,
                                        stem,
                                        optionA,
                                        optionB,
                                        optionC,
                                        optionD,
                                        optionE,
                                        type,
                                        answerKey,
                                        exhibitId,
                                        exhibitPlacement,
                                        exhibitSplit,
                                        stemImage,
                                        imagePlacement,
                                        minOptions,
                                        maxOptions,
                                        supressOptionLabels,
                                        stemHeader,
                                        stemFooter).subscribe(() => {
      this.snackBar.open('Question updated sucessfully', 'OK', {
        duration: 3000,
      });
    });
  }

}
