import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';

import { QuestionService } from '../../question.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent {

  createForm: FormGroup;
  // Form values and variables
  answerKeyValues: any; // TODO: Remove any type.
  singleAnswerType = true;

  // debug mode
  debugMode = true;

  constructor(private questionService: QuestionService, private fb: FormBuilder, private router: Router) {

    // Get default answer fields
    this.answerKeyValues = this.questionService.defaultAnswers;

    this.createForm = this.fb.group({
      itemId: ['', Validators.required],
      stem: ['', Validators.required ],
      optionA: ['', Validators.required ],
      optionB: ['', Validators.required ],
      optionC: '',
      optionD: '',
      optionE: '',
      type: ['', Validators.required ],
      answerKey: this.buildAnswers(),
      exhibitId: '',
      exhibitPlacement: '',
      exhibitSplit: '',
      stemImage: '',
      imagePlacement: '',
      minOptions: ['', Validators.max(25)],
      maxOptions: ['', Validators.max(25)],
      supressOptionLabels: '',
      stemHeader: '',
      stemFooter: ''
    });
  }

  buildAnswers() {
    const arr = this.answerKeyValues.map(answer => {
      return this.fb.control(answer.selected);
    });
    return this.fb.array(arr);
  }

  get answerKey() {
    return this.createForm.get('answerKey');
  }

  addQuestion(itemId,
              stem,
              optionA,
              optionB,
              optionC,
              optionD,
              optionE,
              type,
              answerKey,
              exhibitId,
              exhibitPlacement,
              exhibitSplit,
              stemImage,
              imagePlacement,
              minOptions,
              maxOptions,
              supressOptionLabels,
              stemHeader,
              stemFooter) {

    this.questionService.addQuestion(
      itemId,
      stem,
      optionA,
      optionB,
      optionC,
      optionD,
      optionE,
      type,
      answerKey,
      exhibitId,
      exhibitPlacement,
      exhibitSplit,
      stemImage,
      imagePlacement,
      minOptions,
      maxOptions,
      supressOptionLabels,
      stemHeader,
      stemFooter).subscribe(() => {
      this.router.navigate(['list']);
    });
  }

}
