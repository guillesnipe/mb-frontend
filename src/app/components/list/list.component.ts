import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Question } from '../../question.model';
import { QuestionService } from '../../question.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(private questionService: QuestionService, private router: Router) { }

  questions: Question[];
  displayedColumns = ['stem', 'optionA', 'optionB', 'optionC', 'optionD', 'type', 'processedAnswers', 'actions'];

  ngOnInit() {
    this.fetchIssues();
  }

  fetchIssues() {
    this.questionService
      .getQuestions()
      .subscribe((data: Question[]) => {
        this.questions = data;
        this.questions.map( (question, index) => {
          this.questions[index].processedAnswers = JSON.parse(question.processedAnswers);
        });
      });
  }

  editQuestion(id) {
    this.router.navigate(['edit', `${id}`]);
  }

  deleteQuestion(id) {
    this.questionService.deleteQuestion(id).subscribe(() => {
      this.fetchIssues();
    });
  }

}
