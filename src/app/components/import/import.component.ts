import { Component } from '@angular/core';
import { QuestionService } from '../../question.service';
import { MatSnackBar } from '@angular/material';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent {

  arrayBuffer: any;
  file: File;

  constructor(private questionService: QuestionService,
              private snackBar: MatSnackBar) {}

  incomingfile(event) {
    this.file = event.target.files[0];
  }

  processField(field) {
    field = this.checkAndReplaceImages(field);
    field = this.checkAndReplaceCRSpaces(field);
    field = this.replaceAngleBrackets(field);
    return field;
  }

  checkAndReplaceImages(rawText) {
    const imageRegexp = /<image\s[^>]*?uri\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/gm;
    const match = imageRegexp.exec(rawText);
    if (match !== null) {
      const image = document.createElement('img');
      image.src = `assets/images/${match[1]}`;
      image.alt = match[1];

      return rawText.replace(match[0], image.outerHTML);
    } else {
      return rawText;
    }
  }

  checkAndReplaceCRSpaces(text) {
    // if undefined just return...
    if (typeof text !== 'string') { return text; }

    return text.replace(/(?:\r\n|\r|\n)/g, '<br />');
  }

  // Helper function to escape unsafe characters in a string used to create a new RegExp.
  regExpEscape(regexpString) {
    return String(regexpString).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, '\\$1').
        replace(/\x08/g, '\\x08');
  }

  // Replace Angle brackets in the string passed as parameter, except for img, br and br /
  replaceAngleBrackets(text) {
    // if undefined just return...
    if (typeof text !== 'string') { return text; }

    const htmlRegex = /<(?!img|\/img|br|br\/).*?>/gm;

    const openingAngleBracketRegex = /['<']/g,
          closingAngleBracketRegex = /['>']/g;

    const matches = text.match(htmlRegex);

    if (matches !== null) {
      const uniqueMatches = matches.filter((v, i, a) => a.indexOf(v) === i);
      uniqueMatches.forEach(match => {
        const escapedMatch = match.replace(openingAngleBracketRegex, '&lt;').replace(closingAngleBracketRegex, '&gt;');
        text = text.replace(new RegExp (this.regExpEscape(match), 'g'), escapedMatch);
      });
    }

    return text;
  }

  upload() {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, { type: 'binary' });
      const first_sheet_name = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[first_sheet_name];
      const documents = XLSX.utils.sheet_to_json(worksheet, { raw: true }).map(document => {
        return {
          itemId: document['Item ID'],
          stem: this.processField(document['Stem']),
          optionA: this.processField(document['Option A']),
          optionB: this.processField(document['Option B']),
          optionC: this.processField(document['Option C']),
          optionD: this.processField(document['Option D']),
          optionE: this.processField(document['Option E']),
          type: document['Type'],
          processedAnswers: this.questionService.buildImportedQuestionsAnswers(document['Answer Key'], this.questionService.defaultAnswers),
          exhibitId: document['Exhibit ID']
        };
      });

      console.log(documents);
      // Import questions
      this.importQuestions(documents);
    };

    fileReader.readAsArrayBuffer(this.file);
  }

  importQuestions(questionsArray) {
    // TODO: Unsubscribe
    this.questionService.importQuestions(questionsArray).subscribe(() => {
      this.snackBar.open('Questions imported sucessfully', 'OK', {
        duration: 3000,
      });
    });
  }

  deleteQuestions() {
    // TODO: Unsubscribe
    this.questionService.deleteQuestions().subscribe((data) => {
      console.log(data);
      this.snackBar.open('Questions deleted sucessfully', 'OK', {
        duration: 3000,
      });
    });
  }

}
