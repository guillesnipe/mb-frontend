import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../../question.service';
import { Question } from '../../question.model';

interface PreviewModel {
  question?: number;
  questions?: number;
}

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {

  constructor(private questionService: QuestionService) {}

  questions: Question[];
  currentQuestion: any;

  // Empty state blueprint
  previewStateObject: PreviewModel = {
    question : 0,
    questions : 0
  };

  // Default State
  state: PreviewModel = {};

  initState(questionsAmount) {
    return Object.assign({}, this.previewStateObject, {questions: questionsAmount});
  }

  cleanupState() {
    this.state = {};
  }

  previewReducer (state, action) {
    switch (action.type) {
      case 'NEXT_QUESTION':
        if (state.question + 1 > state.questions) {
          return Object.assign({}, state, {question: 0});
        } else {
          return Object.assign({}, state, {question: state.question + 1});
        }
      case 'PREVIOUS_QUESTION':
        if (state.question - 1 < 0) {
          return Object.assign({}, state, {question: state.questions});
        } else {
          return Object.assign({}, state, {question: state.question - 1});
        }
      default:
        return state;
    }
  }

  nextSlideAction = () => {
    return {
      type: 'NEXT_QUESTION'
    };
  }

  previousSlideAction = () => {
    return {
      type: 'PREVIOUS_QUESTION'
    };
  }

  nextSlide() {
    this.state = this.previewReducer(this.state, this.nextSlideAction());
    console.log(this.state);
    this.setCurrentQuestion(this.state.question);
  }

  previousSlide() {
    this.state = this.previewReducer(this.state, this.previousSlideAction());
    console.log(this.state);
    this.setCurrentQuestion(this.state.question);
  }

  ngOnInit() {
    // Retrieve questions
    this.fetchIssues().subscribe((data: Question[]) => {
      this.questions = data;
      this.questions.map( (question, index) => {
        this.questions[index].processedAnswers = JSON.parse(question.processedAnswers);
      });
      this.state = this.initState(this.questions.length - 1); // Array 0 based
      console.log('State as follows');
      console.log(this.state);
      this.setCurrentQuestion();
    });
  }

  setCurrentQuestion(questionNumber = 0) {
    this.currentQuestion = this.questions[questionNumber];
  }

  fetchIssues() {
    return this.questionService.getQuestions();
  }

  markAnswer(option) {
    let icon = null;
    this.currentQuestion.processedAnswers.every( answer => {
      if (answer.value.toLowerCase() === option.toLowerCase()) {
        icon = '<i class="material-icons green600">done</i>';
        return false;
      } else {
        return true;
      }
    });

    if (!icon) {
      return '<i class="material-icons red600">clear</i>';
    } else {
      return icon;
    }
  }

}
