import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Question } from './question.model';
import { FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) {}

  uri = 'http://localhost:4000';
  // Form values and variables
  answerKeyValues = [
    { id: 0, value: 'A', selected: false },
    { id: 1, value: 'B', selected: false },
    { id: 2, value: 'C', selected: false },
    { id: 3, value: 'D', selected: false },
    { id: 4, value: 'E', selected: false }
  ];

  // Class methods

  buildAnswers(answers, fieldToCheck, formBuilder): FormArray {
    const arr = answers.map(answer => {
      return formBuilder.control(answer[fieldToCheck]);
    });
    return formBuilder.array(arr);
  }

  get defaultAnswers() {
    return JSON.parse(JSON.stringify(this.answerKeyValues));
  }

  getQuestions() {
    return this.http.get(`${this.uri}/questions`);
  }

  getQuestionsById(id) {
    return this.http.get(`${this.uri}/questions/${id}`);
  }

  // This method expects the answerKey (Answers choosen as the right ones.
  // And the default object answerKeyValues, to convert an array of booleans to an array of objects (Questions))
  // Process answers eg: [true, false, true, false, false]
  processQuestionAnswers (answersArray, defaultAnswers) {
    const pristineAnswers = defaultAnswers.slice(); // TODO: Remove this line. Is no longer needed. Replace variable in 'value'
    const processedAnswers = answersArray.map((selected, i) => {
      return {
        id: i,
        value: pristineAnswers[i].value,
        selected
      };
    })
    .filter(answer => answer.selected);

    // Stringify the result
    return JSON.stringify(processedAnswers);
  }

  // Used in the import routine. Given a string of answers like 'a,c' it will insert question objects.
  buildImportedQuestionsAnswers (answersString, defaultAnswers) {
    const answersArray = answersString.split(''); // eg: 'ABCD' -> ['A','B','C','D']
    const convertedAnswers = answersArray.map(answerValue => {
      let answerToBeReturned: any;

      // With every we can loop as much as we want, and exit the loop as soon as we found what we're looking for.
      defaultAnswers.every(answer => {
        if (answer.value === answerValue) {
          answer.selected = true;
          answerToBeReturned = answer;
          return false;
        } else {
          return true;
        }
      });

      return answerToBeReturned;
    });

    return JSON.stringify(convertedAnswers);
  }

  addQuestion(itemId,
              stem,
              optionA,
              optionB,
              optionC,
              optionD,
              optionE,
              type,
              answerKey,
              exhibitId,
              exhibitPlacement,
              exhibitSplit,
              stemImage,
              imagePlacement,
              minOptions,
              maxOptions,
              supressOptionLabels,
              stemHeader,
              stemFooter) {


    // Step one: Convert an array of booleans (answers -> answerKey) into an array of answers.
    const processedAnswers = this.processQuestionAnswers(answerKey, this.answerKeyValues);

    const question: Question = {
      itemId,
      stem,
      optionA,
      optionB,
      optionC,
      optionD,
      optionE,
      type,
      processedAnswers,
      exhibitId,
      exhibitPlacement,
      exhibitSplit,
      stemImage,
      imagePlacement,
      minOptions,
      maxOptions,
      supressOptionLabels,
      stemHeader,
      stemFooter
    };
    return this.http.post(`${this.uri}/questions/add`, question);
  }

  importQuestions(questionsArray) {
    return this.http.post(`${this.uri}/questions/import`, questionsArray);
  }

  deleteQuestions() {
    return this.http.delete(`${this.uri}/questions/delete`);
  }

  updateQuestion (id,
                  itemId,
                  stem,
                  optionA,
                  optionB,
                  optionC,
                  optionD,
                  optionE,
                  type,
                  answerKey,
                  exhibitId,
                  exhibitPlacement,
                  exhibitSplit,
                  stemImage,
                  imagePlacement,
                  minOptions,
                  maxOptions,
                  supressOptionLabels,
                  stemHeader,
                  stemFooter) {

    // Step one: Convert an array of booleans (answers -> answerKey) into an array of answers.
    const processedAnswers = this.processQuestionAnswers(answerKey, this.answerKeyValues);

    const question: Question = {
      itemId,
      stem,
      optionA,
      optionB,
      optionC,
      optionD,
      optionE,
      type,
      processedAnswers,
      exhibitId,
      exhibitPlacement,
      exhibitSplit,
      stemImage,
      imagePlacement,
      minOptions,
      maxOptions,
      supressOptionLabels,
      stemHeader,
      stemFooter
    };
    return this.http.post(`${this.uri}/questions/update/${id}`, question);
  }

  deleteQuestion(id) {
    return this.http.get(`${this.uri}/questions/delete/${id}`);
  }

}
