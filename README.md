# Certification Questions Preview Application

## Setup

Prerequisites: The Magicbox backend server must be up and running.

1 - Clone this repo into your local machine
2 - Install the dependencies by doing `npm install`
3 - Start the app by doing `npm start`

### Uploading images

Some questions will have image/s as its possible answers. If that's the case you will need to upload them to the images folder. (src/assets/images)

1 - Create an `images` folder if it isn't there yet.
2 - Add all the images in that folder.
3 - You're good to go.

### Importing questions

In order to import questions to the app you will need to import the xls file that it's going to be sent to Pearson VUE. Keep in mind that the questions should be on the first page of the xls file.
The url is: localhost:4200/import

## Running Magicbox

1 - Magicbox backend server must be up and running.
2 - In the Magicbox frontend folder do `npm start`
